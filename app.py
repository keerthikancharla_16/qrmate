from flask import Flask, render_template, request
import cv2
app = Flask(__name__)
@app.route('/')
def index():
    return render_template('index.html')
@app.route('/process_qr')
def process_qr():
    cap = cv2.VideoCapture(0)
    cv2.namedWindow('Scanner', cv2.WINDOW_NORMAL)

    while True:
        ret, frame = cap.read()
        if not ret:
            continue
        qr_code = cv2.QRCodeDetector().detectAndDecode(frame)
        if qr_code[0] != '':
            print("QR code recognised and identified!\nDecoded message: ", qr_code[0])
            URL = qr_code[0]
            break
        cv2.imshow('Scanner', frame)
        key = cv2.waitKey(1) & 0xFF
        if key == ord('q'):
            break
    cap.release()
    cv2.destroyAllWindows()
    if qr_code[0] != '':
        return '<a href = "{}">{}</a>'.format(URL,URL)
    return "Unauthorized"
